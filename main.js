// let app = new Vue ({
//     el: '#app',
//     data: {
//         str: 'Hello vue',
//         bindClass: 'bind',
//         stateIf: true,
//         stateElseIf: false,
//         stateShow: false,
//         stateLoop: [
//             'Vue',
//             'React',
//             'Angular',
//             'Ember'
//         ],
//         // v-bind
//         img: {
//             src: 'https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjmk4Cu6PfgAhUYwcQBHbxtCeEQjRx6BAgBEAU&url=https%3A%2F%2Ftwitter.com%2Fvuenewsletter&psig=AOvVaw0NRNXchsGdzMgkHD6m1bEC&ust=1552315588253751',
//             alt: 'Vue.js course'
//         },
//         dataAttr: true,
//         toggler: true
//     }
// })
// let www = new Vue ({
//     el: '#test',
//     data: {
//         count: 1,
//         arr: ['aaa', 
//         'bbbb']
//     }
// })

let homework = new Vue ({
    el: '#homework',
    data: {
        toggler: ["aaa1", "aaa2", "aaa3"],
        arr1: ["aaa1", "aaa2", "aaa3"],
        arr2: ["bbb1", "bbb2", "bbb3"],
        arr3: ["ccc1", "ccc2", "ccc3"],
        color: 'bind',
    }
})